import sys

import moulinette


def main(argc, argv):
    if argc != 2:
        print("usage: python3 moulinette.py <script>")
        return 1
    files_errors = moulinette.process_path(argv[1])
    nberr = moulinette.print_moulinette(files_errors)
    return 0 if nberr == 0 else 1


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
