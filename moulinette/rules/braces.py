from ..tools import match
from ..rule import Rule, RuleType


def rule_braces(line, f):
    if match(line, "^([ ]*({|})(,|;)?)$"):
        # Bracket is correct
        return 1
    elif match(line, "^.*([\"]|[']){1}.*({|}).*([\"]|[']){1}.*$"):
        # Bracket is in string, thus ignored
        return 1
    elif match(line, "^[ ]*({|})(,|;)?[ ]*[\\\]"):
        # Bracket is in MACRO function
        return 1
    elif match(line, '^.*({|}).*$'):
        return 0
    else:
        return 1


Rule.register(Rule(rule_braces, RuleType.LINE, "All braces MUST be on their own line. (6.1)"))
