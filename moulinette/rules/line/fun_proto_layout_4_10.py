from moulinette.tools import is_function_prototype
from moulinette.rule import Rule, RuleType


def function_proto_layout(line, f):
    if is_function_prototype(line) and ".c" in f.path:
        return False
    return True


Rule.register(Rule(function_proto_layout, RuleType.LINE,
                   "Prototypes for exported function MUST appear in header files and MUST NOT appear in source files. (4.10)"))
