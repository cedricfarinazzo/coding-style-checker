from moulinette.tools import match
from moulinette.rule import Rule, RuleType


def exp_nopadding_arrow(line, f):
    if match(line, ".*((\w+ +->\w+)|(\w+-> +\w+)|(\w+ +-> +\w+)).*"):
        return False
    return True


Rule.register(Rule(exp_nopadding_arrow, RuleType.LINE,
                   "The -> operator MUST NOT be padded. (5.5)"))

def exp_nopadding_point(line, f):
    if match(line, ".*((\w+ +\.\w+)|(\w+\. +\w+)|(\w+ +\. +\w+)).*"):
        return False
    return True


Rule.register(Rule(exp_nopadding_point, RuleType.LINE,
                   "The . operator MUST NOT be padded. (5.5)"))
