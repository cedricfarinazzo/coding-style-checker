from moulinette.rule import Rule, RuleType


def file_trailing(line, f):
    line = line.replace('\n', '')
    return line == line.rstrip()


Rule.register(Rule(file_trailing, RuleType.LINE,
                   "There MUST NOT be any whitespace at the end of a line. (3.7)"))
