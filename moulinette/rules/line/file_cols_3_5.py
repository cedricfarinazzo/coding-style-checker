from moulinette.rule import Rule, RuleType


def file_cols(line, f):
    return len(line) <= 81


Rule.register(Rule(file_cols, RuleType.LINE,
                   "Lines MUST NOT exceed 80 columns in width, excluding the trailing newline character. (3.5)"))
