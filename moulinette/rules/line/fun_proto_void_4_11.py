from moulinette.tools import is_function_declaration, match
from moulinette.rule import Rule, RuleType


def function_proto_void(line, f):
    if is_function_declaration(line):
        if match(line, "^((struct *)?)[\w-]+ +(\*?)[\w-]+ *\(\)[^;\s]*$"):
            return False
    return True


Rule.register(Rule(function_proto_void, RuleType.LINE,
                   "Prototypes MUST specify void if your function does not take any argument. (4.11)"))
