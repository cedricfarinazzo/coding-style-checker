from moulinette.rule import Rule, RuleType


def keyword_goto(line, f):
    if 'goto' in line:
        return False
    return True


Rule.register(Rule(keyword_goto, RuleType.LINE,
                   "The goto statement MUST NOT be used. (5.26)"))
