from moulinette.rule import Rule, RuleType


def name_struct(line, f):
    if "typedef" in line and ("struct" in line or "union" in line):
        return False
    return True


Rule.register(Rule(name_struct, RuleType.LINE,
                   "Structure and union names MUST NOT be aliased using typedef. (2.5)"))
