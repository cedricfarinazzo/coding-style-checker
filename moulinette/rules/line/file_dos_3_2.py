from moulinette.rule import Rule, RuleType


def file_dos(line, f):
    return '\r\n' not in line


Rule.register(Rule(file_dos, RuleType.LINE,
                   "The DOS CR+LF line terminator MUST NOT be used. (3.2)"))
