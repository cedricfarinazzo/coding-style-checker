from moulinette.tools import match
from moulinette.rule import Rule, RuleType


def cast(line, f):
    if match(line, ".*= *\([\w]+( *\*)?\).*"):
        return False
    return True


Rule.register(Rule(cast, RuleType.LINE,
                   "The number of allowed C cast is 0. (4.5)"))
