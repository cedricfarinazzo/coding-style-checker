from moulinette.rule import Rule, RuleType


def file_indentation(line, f):
    return '\t' not in line


Rule.register(Rule(file_indentation, RuleType.LINE,
                   "Indentation MUST be done using whitespaces only, tabulations MUST NOT appear in your source code. (3.4)"))
