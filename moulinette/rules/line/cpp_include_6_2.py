from moulinette.tools import match, is_include
from moulinette.rule import Rule, RuleType


def cpp_include_bad_file(line, f):
    if is_include(line) and match(line, " *# *include *(<|\").*\.[^h]+(>|\") *$"):
        return False
    return True


Rule.register(Rule(cpp_include_bad_file, RuleType.LINE,
                   "#include of others file than header (.h) are not allowed. (6.2)"))

def cpp_include_relative(line, f):
    if is_include(line) and match(line, " *# *include *(<|\")(((\.\.)|(\.))/)+.*(>|\") *$"):
        return False
    return True


Rule.register(Rule(cpp_include_bad_file, RuleType.LINE,
                   "Relative #include are not allowed. (6.2)"))
