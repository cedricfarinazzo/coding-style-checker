from moulinette.tools import is_prepocessor
from moulinette.rule import Rule, RuleType


def cpp_mark(line, f):
    if is_prepocessor(line):
        return line[0] == '#'
    return True


Rule.register(Rule(cpp_mark, RuleType.LINE,
                   "The preprocessor directive mark (#) MUST appear on the first column. (6.7)"))
