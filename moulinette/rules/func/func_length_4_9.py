from moulinette.tools import search_opening_brackets, search_closing_brackets
from moulinette.rule import Rule, RuleType


def function_lenght(func, f):
    s = search_opening_brackets(func, 0)
    e = search_closing_brackets(func, 0)

    count = 0
    for i in range(s, e):
        l = func[i].strip()
        if len(l) == 0 or '{' in l or '}' in l:
            continue
        count += 1

    return count <= 25


Rule.register(Rule(function_lenght, RuleType.FUNCTION, "More than 25 lines in function. (4.9)"))
