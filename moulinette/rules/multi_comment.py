from ..tools import match
from ..rule import Rule, RuleType


def rule_multi_comments(line, f):
    if match(line, "^(([ ]?\*[ ])|([ ]?\*$))"):
        return 0
    return 1


Rule.register(Rule(rule_multi_comments, RuleType.LINE, "Intermediary lines start with ** (5.9)"))
