from moulinette.rule import Rule, RuleType


def file_spurious(lines, f):
    try:
        return lines[-1].rstrip().replace('\n', '') != '' and lines[0].rstrip().replace('\n', '') != ''
    except Exception:
        return False


Rule.register(Rule(file_spurious, RuleType.FILE,
                   "There MUST NOT be any blank lines at the beginning or the end of the file. (3.6)"))
