from moulinette.rule import Rule, RuleType


def file_terminate(lines, f):
    try:
        return lines[-1].rstrip().replace('\n', '') != ''
    except Exception:
        return False


Rule.register(Rule(file_terminate, RuleType.FILE,
                   "The last line of this file MUST NOT be a single line feed. (3.3)"))
