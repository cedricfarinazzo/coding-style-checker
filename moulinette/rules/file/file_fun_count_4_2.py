from moulinette.tools import is_function_declaration
from moulinette.rule import Rule, RuleType


def function_count(lines, f):
    count = 0
    for line in lines:
        if is_function_declaration(line):
            count += 1
    return count <= 10


Rule.register(Rule(function_count, RuleType.FILE, "More than 10 functions in this file. (4.2)"))
