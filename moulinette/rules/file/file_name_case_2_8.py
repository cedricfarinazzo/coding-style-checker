import os
from moulinette.tools import match
from moulinette.rule import Rule, RuleType


def file_name_case(lines, f):
    if not match(os.path.splitext(os.path.basename(f.path))[0], "[a-z][a-z0-9_]*"):
        return False
    return True

Rule.register(Rule(file_name_case, RuleType.FILE, "Filename SHOULD be matched by the following regular expression: [a-z][a-z0-9_]* (2.8)"))
