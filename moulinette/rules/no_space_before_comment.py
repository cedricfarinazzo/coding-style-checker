from ..tools import match
from ..rule import Rule, RuleType


def rule_no_space_before_comment(line, f):
    if match(line, '^([ ]+[\*]+.*)'):
        return 0
    return 1


Rule.register(Rule(rule_no_space_before_comment, RuleType.LINE, "Comments must not be indented (5.9 bis)"))
