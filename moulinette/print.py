from .rule import rules
from .tools import warn, fail, info


def print_moulinette(files):
    print('\n' + info('{ Rules }'.center(80, '=')) + '\n')
    print(f'- Number of rules: {len(rules)}\n')
    for i in range(len(rules)):
        rule = rules[i]
        print(f'- {i}: {rule.type}  {rule.message}')
        
    print('\n' + info(''.center(80, '=')) + '\n')

    print('\n' + fail('{ REPORT }'.center(80, '=')) + '\n')
    
    nb_err = 0
    for file in files:
        if not file.error:
            continue
        print('\n' + info(('{ ' + file.path + ' }').center(80, '-')) + '\n')
        for err in file.error:
            print(warn("Line: " + str(err.line + 1)))
            print(err.message)
            nb_err += 1
    
    if nb_err == 0:
        print("Awesome! No coding style error found!\n")

    print('\n' + fail('{ STATS }'.center(80, '=')) + '\n')
    print('Total: ' + fail(str(nb_err)) + ' error found\n')
    return nb_err
