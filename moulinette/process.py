import os

from .config import *
from .data import *
from .tools import *
from .rule import *
from .rules import *


def process_file(p):
    filedata = FileData(path=p, error=[])
    with open(filedata.path, "r") as handler:
        filedata.data = handler.readlines()
    filedata.lines_count = len(filedata.data)
    Rule.exec(filedata)
    return filedata


def process_path(p):
    files = []
    if os.path.isdir(p):
        for root, dirs, filenames in os.walk(p):
            if match(root, Config.ignore):
                dirs[:] = []
                filenames[:] = []
            else:
                for f in filenames:
                    f = str(f)
                    if match(f, Config.chk_file):
                        files.append(process_file(os.path.join(root, f)))
    else:
        if match(p, Config.chk_file):
            files.append(process_file(p))
    return files
