from .data import CodingStyleError, FileData
from .tools import get_function_index

import enum


class RuleType(enum.Enum):
    FILE = 0
    LINE = 1
    FUNCTION = 2


rules = []


class Rule:
    message = None
    callback = None
    type = None

    def __init__(self, callback, type, message):
        self.callback = callback
        self.type = type
        self.message = message

    @staticmethod
    def exec(filedata):
        # FILE CHECK
        for rule_file in rules:
            if rule_file.type != RuleType.FILE:
                continue
            if not rule_file.callback(filedata.data, filedata):
                filedata.error.append(CodingStyleError(rule_file.message, 0, filedata.path))

        # LINE CHECK
        for rule_line in rules:
            if rule_line.type != RuleType.LINE:
                continue
            for i in range(len(filedata.data)):
                if not rule_line.callback(filedata.data[i], filedata):
                    filedata.error.append(CodingStyleError(rule_line.message, i, filedata.path))

        # FUNCTION CHECK
        s, e = get_function_index(filedata)
        while s is not None and e is not None:
            for rule_func in rules:
                if rule_func.type != RuleType.FUNCTION:
                    continue
                if not rule_func.callback(filedata.data[s:e], filedata):
                    filedata.error.append(CodingStyleError(rule_func.message, s, filedata.path))
            s, e = (get_function_index(filedata, start=e))

    @staticmethod
    def register(rule):
        rules.append(rule)
