import re

from .config import C


def info(msg):
    return C.INFO + C.BOLD + msg + C.ENDC


def warn(msg):
    return C.WARNING + msg + C.ENDC


def fail(msg):
    return C.FAIL + msg + C.ENDC


def match(line, regex):
    reg = re.compile(regex)
    if re.match(reg, line):
        return 1
    return 0

def is_prepocessor(line):
    return match(line, "^ *#.*$")

def is_include(line):
    return is_prepocessor(line) and match(line, "^ *# *include.*$")

def is_function_prototype(line):
    return match(line,
                 ".*((struct *)?)[\w-]+ +(\*?)[\w-]+ *\(((void)|(([\w-]+ +[\w-]+)(, *([\w-]+ +[\w-]+))*))?\) *;.*")

def is_function_declaration(line):
    return match(line,
                 "^((struct *)?)[\w-]+ +(\*?)[\w-]+ *\(((void)|(([\w-]+ +[\w-]+)(, *([\w-]+ +[\w-]+))*))?\)[^;\s]*$")


def numberofx(s, x):
    c = 0
    for e in s:
        c += e == x
    return c


def search_opening_brackets(lines, i):
    while i < len(lines):
        if numberofx(lines[i], '{') > 0:
            return i + 1
        i += 1
    return i


def search_closing_brackets(lines, i):
    op_b = 0
    cl_b = 0
    while (op_b == 0 and cl_b == 0) or (op_b != cl_b):
        if i >= len(lines):
            break
        line = lines[i];
        op_b = op_b + numberofx(line, '{')
        cl_b = cl_b + numberofx(line, '}')
        i += 1
    return i


def get_function_index(f, start=0):
    for i in range(start, f.lines_count):
        line = f.data[i]
        if is_function_declaration(line):
            return i, search_closing_brackets(f.data, i)

    return None, None
