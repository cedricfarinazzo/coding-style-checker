from .tools import info, warn, fail

class CodingStyleError:
    message = None
    line = 0
    path = None

    def __init__(self, message, line, path):
        self.message = message
        self.line = line
        self.path = path


class FileData:

    def __init__(self, path=None, data=None, lines_count=0, error=[], cache={}):
        self.data = data
        self.path = path
        self.lines_count = lines_count
        self.error = error
        self.cache = cache
