class Config:
    chk_file = '^.+[.](c|h)$'
    ignore = '^.*((.git/|build/|test/|tests/).*|(.git|build|test|tests))$'


class C:
    HEADER = '\033[95m'
    INFO = '\033[94m'
    SUCCESS = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    ENDC = '\033[0m'
